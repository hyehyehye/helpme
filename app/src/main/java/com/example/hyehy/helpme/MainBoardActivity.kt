package com.example.hyehy.helpme

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.widget.FrameLayout
import android.widget.Toast
import com.example.hyehy.helpme.common.BottomNavigationHelper
import com.example.hyehy.helpme.common.ExtendedActivity
import com.example.hyehy.helpme.fragment.BudgetFragment
import com.example.hyehy.helpme.fragment.CategoryFragment
import com.example.hyehy.helpme.fragment.DailyUsageFragment
import com.example.hyehy.helpme.fragment.MonthlyUsageFragment
import com.example.hyehy.helpme.setting.MainBottomMenu

class MainBoardActivity : ExtendedActivity(), MainBottomMenu.BottomSheetListener{
    override fun resetLocation() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    val bottomNavigation: BottomNavigationView by lazy { findViewById<BottomNavigationView>(R.id.bottom_navigation) }
    val layoutContent by lazy { findViewById<FrameLayout>(R.id.container_main_content) }

    companion object {
        @JvmStatic
        lateinit var sBottomNavigation: BottomNavigationView
    }

    val mCompanion = Companion
    val menuUserID : String = " "


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mCompanion.sBottomNavigation = bottomNavigation
        BottomNavigationHelper.removeShiftMode(bottomNavigation)

        bottomNavigation.setOnNavigationItemSelectedListener { it ->
            when (it.itemId) {
                R.id.action_monthly_usage -> {
                    supportFragmentManager
                            .beginTransaction()
                            .replace(layoutContent.id, MonthlyUsageFragment.getInstance())
                            .commit()
                }
                R.id.action_daily_usage -> {
                    supportFragmentManager
                            .beginTransaction()
                            .replace(layoutContent.id, DailyUsageFragment.getInstance())
                            .commit()
                }
                R.id.action_budget -> {
                    supportFragmentManager
                            .beginTransaction()
                            .replace(layoutContent.id, BudgetFragment.getInstance())
                            .commit()
                }
                R.id.action_category -> {
                    supportFragmentManager
                            .beginTransaction()
                            .replace(layoutContent.id, CategoryFragment.getInstance())
                            .commit()
                }
                R.id.action_setting -> {
                    MainBottomMenu().apply {
                    }.also { it.show(supportFragmentManager, "mainBottomSheetDialog") }
                    bottomNavigation.getChildAt(0).performClick()

                    return@setOnNavigationItemSelectedListener false
                }
                else -> {}
            }

            true
        }

        supportFragmentManager
                .beginTransaction()
                .replace(layoutContent.id, MonthlyUsageFragment())
                .commit()
    }

    override fun onButtonClicked(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }

}