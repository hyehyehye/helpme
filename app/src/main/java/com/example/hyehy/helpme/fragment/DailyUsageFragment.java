package com.example.hyehy.helpme.fragment;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.TextView;

import com.example.hyehy.helpme.DailyUsageList;
import com.example.hyehy.helpme.DailyUsageListAdapter;

import com.example.hyehy.helpme.HandinputActivity;
import com.example.hyehy.helpme.LoginActivity;
import com.example.hyehy.helpme.MainBoardActivity;
import com.example.hyehy.helpme.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class DailyUsageFragment extends Fragment {
    private static final String PATTERN_DATE_MONTH = "M";
    private static final String PATTERN_DATE_DATE = "d";
    public static DailyUsageFragment INSTANCE = null;
    static CoordinatorLayout mContainer = null;

    Button fakeLogin;
    Button btnHandInputUsage;
    TextView tvDay;
    TextView tvPrevDay;
    TextView tvNextDay;
    String thisDayString;
    String thisMonthString;
    int thisDayInt1;
    int thisMonthInt1;
    int year;

    Calendar calendar;

    ListView listviewDailyUsage;
    DailyUsageListAdapter dailyUsageListAdapter;
    ArrayList<DailyUsageList> list_dailyUsageArrayList;


    public static DailyUsageFragment getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new DailyUsageFragment();
        }
        return INSTANCE;
    }


    @Override
    public CoordinatorLayout onCreateView(LayoutInflater inflater, ViewGroup container,
                                          Bundle savedInstanceState) {
        if (mContainer == null) {
            mContainer = (CoordinatorLayout) inflater.inflate(R.layout.fragment_daily_usage, null);
        }

        fakeLogin = mContainer.findViewById(R.id.fakelogin);
        btnHandInputUsage = mContainer.findViewById(R.id.btn_hand_input_usage);
        tvDay = mContainer.findViewById(R.id.tv_day);
        tvPrevDay = mContainer.findViewById(R.id.tv_prev_day);
        tvNextDay = mContainer.findViewById(R.id.tv_next_day);

        calendar = Calendar.getInstance();

        //나중에 메인화면으로 빼고 로그인 버튼은 없애면 됨
        fakeLogin.setOnClickListener((View v1) -> {
            Intent loginIntent = new Intent(getContext(), LoginActivity.class);
            ((MainBoardActivity) getActivity()).startActivity(loginIntent);
        });

        //지출 수동입력 버튼
        btnHandInputUsage.setOnClickListener(v1 -> {
            Intent handInput = new Intent(getContext(), HandinputActivity.class);
            ((MainBoardActivity) getActivity()).startActivity(handInput);
        });


        //현재 월, 일 설정 (int, String)
        thisMonthString = getDateString(PATTERN_DATE_MONTH);
        thisDayString = getDateString(PATTERN_DATE_DATE);

        thisMonthInt1 = Integer.parseInt(thisMonthString);
        thisDayInt1 = Integer.parseInt(thisDayString);

        //현재 날짜로 기본 설정
        tvDay.setText(thisMonthString + "월  " + thisDayString + "일");

        //하루 전으로 이동
        tvPrevDay.setOnClickListener(v1 -> {
            thisDayInt1 -= 1;
            calcDate();
        });

        //하루 뒤로 이동
        tvNextDay.setOnClickListener(v1 -> {
            thisDayInt1 += 1;
            calcDate();
        });


        //날짜 보여주는 textview를 누르면 날짜 선택할 수 있는 datePicker가 나오게 하고싶었는데
        //클릭하면 잘 보이긴 하는데 그 뒤에 ◁▷로 이동할 경우 날짜가 안맞음ㅠㅠ 변수를 아무때나 막 쓰는게 문제인거같은데
        tvDay.setOnClickListener((View v1) -> {
            year = calendar.get(Calendar.YEAR);
            thisMonthInt1 = calendar.get(Calendar.MONTH);
            thisDayInt1 = calendar.get(Calendar.DAY_OF_MONTH);
            setDatePicker();
        });

        //하단 리스트뷰 내용
        listviewDailyUsage = mContainer.findViewById(R.id.listView_daily_usage);
        listviewDailyUsage.setAdapter(generateAdapter());

        return mContainer;
    }



    private void setDatePicker() {
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int thisMonthInt1, int thisDayInt1) {
                thisMonthString = Integer.toString(thisMonthInt1+1);
                thisDayString = Integer.toString(thisDayInt1);

                tvDay.setText(thisMonthString + "월  " + thisDayString + "일");
            }
        },year, thisMonthInt1, thisDayInt1);
        datePickerDialog.show();
    }

    //리스트뷰에 추가되는 내용은 여기에
    private DailyUsageListAdapter generateAdapter() {
        list_dailyUsageArrayList = new ArrayList<DailyUsageList>();
        list_dailyUsageArrayList.add(new DailyUsageList("이디야 흑당버블라떼", "4500원"));
        list_dailyUsageArrayList.add(new DailyUsageList("서울우유", "2500원"));
        list_dailyUsageArrayList.add(new DailyUsageList("스타벅스 아이스아메리카노", "4100원"));
        list_dailyUsageArrayList.add(new DailyUsageList("이디야 아이스티", "3000원"));
        list_dailyUsageArrayList.add(new DailyUsageList("케찹", "1000원"));
        list_dailyUsageArrayList.add(new DailyUsageList("인간공학 교재", "33000원"));
        list_dailyUsageArrayList.add(new DailyUsageList("밀가루", "3000원"));
        list_dailyUsageArrayList.add(new DailyUsageList("선풍기", "1500000원"));

        return  new DailyUsageListAdapter(getActivity(), list_dailyUsageArrayList);
    }

    private String getDateString(String pattern) {
        DateFormat dateFormat = new SimpleDateFormat(pattern);
        Date nowDate = new Date();
        return dateFormat.format(nowDate);
    }

    private void calcDate() {
        calcMonthDaty();
        thisDayString = Integer.toString(thisDayInt1);
        thisMonthString = Integer.toString(thisMonthInt1);
        tvDay.setText(thisMonthString + "월  " + thisDayString + "일");
    }


    public void calcMonthDaty() {
        //각 월에 맞는 날짜 설정
        switch (thisMonthInt1) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                DayCalc31();
                break;
            case 2:
            case 4:
            case 6:
            case 9:
            case 11:
                DayCalc30();
                break;
        }
    }

    public void calcMonth() {
        //12월 뒤에 1월이 오도록. 1월 전에는 12월
        if (thisMonthInt1 < 1) {
            thisMonthInt1 = 12;
        } else if (thisMonthInt1 > 12) {
            thisMonthInt1 = 1;
        }
    }

    //31일까지 있는 달
    public void DayCalc31() {
        if (thisDayInt1 < 1 && thisMonthInt1 != 8) {
            thisDayInt1 = 30;
            thisMonthInt1 -= 1;
            calcMonth();
        } else if (thisDayInt1 < 1 && thisMonthInt1 == 8) {
            thisDayInt1 = 31;
            thisMonthInt1 += 1;
            calcMonth();
        } else if (thisDayInt1 > 31) {
            thisDayInt1 = 1;
            thisMonthInt1 += 1;
            calcMonth();
        }
    }

    //30일까지 있는 달
    public void DayCalc30() {
        if (thisDayInt1 < 1) {
            thisDayInt1 = 31;
            thisMonthInt1 -= 1;
            calcMonth();
        } else if (thisDayInt1 > 30) {
            thisDayInt1 = 1;
            thisMonthInt1 += 1;
            calcMonth();
        }
    }
}


