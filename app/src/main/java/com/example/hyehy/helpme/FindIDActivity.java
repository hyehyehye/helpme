package com.example.hyehy.helpme;

import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.hyehy.helpme.model.Message;
import com.example.hyehy.helpme.remote.Request;
import com.example.hyehy.helpme.remote.RetrofitAPI;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FindIDActivity extends AppCompatActivity {

    EditText inputName;
    EditText inputPhone;
    Button buttonFindID;
    Button btnBack;
    private AlertDialog dialog1;


    @Override
    protected void onStop() {
        super.onStop();
        if (dialog1 != null) {
            dialog1.dismiss();
            dialog1 = null;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_find_id);

        inputName = findViewById(R.id.input_find_name);
        inputPhone = findViewById(R.id.input_find_phone_number);
        buttonFindID = findViewById(R.id.button_find_id);
        btnBack = findViewById(R.id.button_back);


        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        //아이디찾기 버튼 누를 시
        buttonFindID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Request request = RetrofitAPI.getApICall();
                final String name1 = inputName.getText().toString();
                final String phone1 = inputPhone.getText().toString();
                if (inputName.length() == 0) {
                    inputName.setError("이름을 입력하세요.");
                    inputName.requestFocus();
                    return;
                } else if (phone1.isEmpty()) {
                    inputPhone.setError("핸드폰 번호를 입력하세요.");
                    return;

                }
                Call<Message> findidcall = request.findUserid(name1, phone1);

                findidcall.enqueue(new Callback<Message>() {
                    @Override
                    public void onResponse(Call<Message> call, Response<Message> response) {
                        if (response.isSuccessful()) { //성공한 경우
                            Message message = response.body();

                            if (response.code() == 200) {

                                AlertDialog.Builder builder = new AlertDialog.Builder(FindIDActivity.this);
                                dialog1 = builder.setMessage("" + message.getMessage().toString()).setNegativeButton("확인", null).create();
                                dialog1.show();
                            }
                        } else { //실패한 경우
                            if (response.code() == 500) {
                                showToast("이름 혹은 전화번호가 틀렸습니다.");
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<Message> call, Throwable t) {
                        showToast("Failer에서 응답");
                    }
                });

            }
        });
    }

    void showToast(String msg) {
        Toast.makeText(this, "" + msg, Toast.LENGTH_LONG).show();
    }

}



