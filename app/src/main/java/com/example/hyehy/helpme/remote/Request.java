package com.example.hyehy.helpme.remote;

import com.example.hyehy.helpme.model.Budget;
import com.example.hyehy.helpme.model.BudgetStatus;
import com.example.hyehy.helpme.model.CategoryData;
import com.example.hyehy.helpme.model.DayList;
import com.example.hyehy.helpme.model.JWTToken;
import com.example.hyehy.helpme.model.Message;
import com.example.hyehy.helpme.model.MonthPercent;
import com.example.hyehy.helpme.model.MonthValue;
import com.example.hyehy.helpme.model.PrevCompare;
import com.example.hyehy.helpme.model.PurchaseList;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

public interface Request {

    @GET("history/excelmobile")
    Call<Message> requestExcel(
            @Header("x-access-token") String token,
            @Query("year") int year,
            @Query("month") int month
    );

    //로그인
    @FormUrlEncoded
    @POST("auth/login")
    Call<JWTToken> userlogin(
            @Field("email") String email,
            @Field("password") String password
    );

    //회원가입
    @POST("auth/reg")
    @FormUrlEncoded
    Call<Message> registerUser(
            @Field("name") String name,
            @Field("email") String email,
            @Field("phone") String phone,
            @Field("password") String password,
            @Field("coupang_id") String coupid,
            @Field("coupang_pw") String couppw
    );

    //아이디찾기
    @POST("auth/search/id")
    @FormUrlEncoded
    Call<Message> findUserid(
            @Field("name") String name,
            @Field("phone") String phone
    );

    //비번찾기
    @POST("auth/search/pw")
    @FormUrlEncoded
    Call<Message> findUserpw(
            @Field("name") String name,
            @Field("email") String email,
            @Field("phone") String phone
    );

    //회원탈퇴
    @DELETE("user/withdrawl")
    Call<Message> outreq(
            @Header("x-access-token") String token,
            @Field("pass") String pass
    );

    @POST("/user/delete")
    @FormUrlEncoded
    Call<Message> deleteAcount(
            @Field("id") String id
    );

    @FormUrlEncoded
    @HTTP(method = "DELETE", path = "/user/withdrawl", hasBody = true)
    Call<Message> userOut(
            @Header("x-access-token") String token,
            @Field("password") String password
    );

    //문의하기
    @POST("/user/feedback")
    @FormUrlEncoded
    Call<Message> questionReq(
            @Header("x-access-token") String token,
            @Field("content") String content
    );


    //예산
    @GET("/history/budget")
    Call<BudgetStatus> requestBudgetStatus(
            @Header("x-access-token") String token
    );

    //예산
    @GET("user/budget")
    Call<Budget> requestBudget(
            @Header("x-access-token") String token
    );

    //예산
    @FormUrlEncoded
    @PUT("/user/budget")
    Call<Budget> postBudget(
            @Header("x-access-token") String token,
            @Field("budget") int budget
    );

    //일별지출
    @GET("history/day")
    Call<DayList> DayListCall(
            @Header("x-access-token") String token,
            @Query("year") int year,
            @Query("month") int month,
            @Query("day") int day
    );

    //수동 입력하는 부분
    @FormUrlEncoded
    @POST("/history/new")
    Call<PurchaseList> InputPurchase(
            @Header("x-access-token") String token,
            @Field("name") String name,
            @Field("price") int price,
            @Field("date") String date
    );

    //월별
    @GET("/history/month")
    Call<MonthValue> MontlyList(
            @Header("x-access-token") String token,
            @Query("year") int year,
            @Query("month") int month
    );

    //지난달과 사용량 비교
    @GET("/history/prev")
    Call<PrevCompare> PrevCompareCall(
            @Header("x-access-token") String token,
            @Query("month") int month
    );

    //카테고리별 점유율 퍼센트
    @GET("/history/percent")
    Call<MonthPercent> PercentList(
            @Header("x-access-token") String token
    );

    //카테고리별 사용 금액
    @GET("/history/category")
    Call<CategoryData> requestCategoryPrice(
            @Header("x-access-token") String token,
            @Query("category") String category
    );


}



