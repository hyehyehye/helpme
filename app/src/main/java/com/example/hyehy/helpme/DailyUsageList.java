package com.example.hyehy.helpme;

public class DailyUsageList {

    private String dailyListName;
    private String dailyListCost;


    public DailyUsageList(String dailyListName, String dailyListCost) {
        this.dailyListName = dailyListName;
        this.dailyListCost = dailyListCost;
    }

    public String getDailyListName() {
        return dailyListName;
    }

    public void setDailyListName(String dailyListName) {
        this.dailyListName = dailyListName;
    }

    public String getDailyListCost() {
        return dailyListCost;
    }

    public void setDailyListCost(String dailyListCost) {
        this.dailyListCost = dailyListCost;
    }

}
