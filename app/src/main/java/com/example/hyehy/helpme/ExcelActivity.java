package com.example.hyehy.helpme;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.hyehy.helpme.model.Message;
import com.example.hyehy.helpme.remote.Request;
import com.example.hyehy.helpme.remote.RetrofitAPI;
import com.example.hyehy.helpme.tokenmanager.TokenManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ExcelActivity extends AppCompatActivity {
    Integer year;
    Integer month;
    private TokenManager tokenManager;
    private AlertDialog dialog1;

    EditText editYear;
    EditText editMonth;
    Button btnExcel;
    ImageButton btnBack;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_excel);

        editYear = findViewById(R.id.yearInput);
        editMonth = findViewById(R.id.monthInput);
        btnExcel = findViewById(R.id.btnExcel);
        btnBack = findViewById(R.id.btnBack_excel);

        tokenManager = new TokenManager(getApplicationContext());
        String token = tokenManager.getto();


        btnExcel.setOnClickListener((v) -> {
            if (editYear.getText().length() == 0 || editMonth.getText().length() == 0)
                showToast("연도와 월을 모두 입력해주세요");
            else {
                final Request apiCall = RetrofitAPI.getApICall();
                year = new Integer(editYear.getText().toString());
                month = new Integer(editMonth.getText().toString());
                Call<Message> excelcall = apiCall.requestExcel(token, year, month);
                excelcall.enqueue(new Callback<Message>() {
                    @Override
                    public void onResponse(Call<Message> call, Response<Message> response) {
                        if (response.code() == 200) {
                            Message message = response.body();
                            AlertDialog.Builder builder = new AlertDialog.Builder(ExcelActivity.this);
                            dialog1 = builder.setMessage("엑셀 파일이 메일로 전송되었습니다.").setNegativeButton("확인", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    Intent Ok = new Intent(getApplicationContext(), MainActivity.class);
                                    startActivity(Ok);
                                    finish();
                                }
                            }).create();
                            dialog1.show();
                        } else if (response.code() == 501) {
                            showToast("결과없음");
                        } else if (response.code() == 500) {
                            showToast("서버에러");
                        }
                    }

                    @Override
                    public void onFailure(Call<Message> call, Throwable t) {
                        showToast("네트워크 통신 상태를 확인하세요.");
                    }
                });
            }

        });


        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    void showToast(String msg) {
        Toast.makeText(this, "" + msg, Toast.LENGTH_LONG).show();
    }
}

