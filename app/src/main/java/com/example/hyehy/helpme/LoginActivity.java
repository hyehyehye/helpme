package com.example.hyehy.helpme;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hyehy.helpme.tokenmanager.TokenManager;
import com.example.hyehy.helpme.model.JWTToken;
import com.example.hyehy.helpme.remote.Request;
import com.example.hyehy.helpme.remote.RetrofitAPI;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.util.Log.d;

public class LoginActivity extends AppCompatActivity {
    private long time = 0;
    private TokenManager tokenManager;
    private AlertDialog dialog;

    EditText inputId;
    EditText inputPw;
    Button buttonLogin;
    TextView textJoinBtn;
    TextView textFindIdBtn;
    TextView textFindPwBtn;


    @Override
    protected void onStop() {
        super.onStop();
        if (dialog != null) {
            dialog.dismiss();
            dialog = null;
        }
    }

    @Override
    public void onBackPressed() {
        if (System.currentTimeMillis() - time >= 2000) {
            time = System.currentTimeMillis();
            Toast.makeText(getApplicationContext(), "한번 더 누르시면 종료됩니다", Toast.LENGTH_LONG).show();
        } else if (System.currentTimeMillis() - time < 2000) {
            finish();
            return;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_login);

        tokenManager = new TokenManager(getApplicationContext());

        inputId = findViewById(R.id.input_id);
        inputPw = findViewById(R.id.input_pw);
        buttonLogin = findViewById(R.id.button_login);
        textJoinBtn = findViewById(R.id.text_join_button);
        textFindIdBtn = findViewById(R.id.text_find_id_button);
        textFindPwBtn = findViewById(R.id.text_find_pw_button);

        //회원가입
        textJoinBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent registerIntent = new Intent(getApplicationContext(), JoinActivity.class);
                startActivity(registerIntent);
            }
        });

        //아이디찾기
        textFindIdBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent findidIntent = new Intent(getApplicationContext(), FindIDActivity.class);
                startActivity(findidIntent);
            }
        });

        //비번찾기
        textFindPwBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent findpwIntent = new Intent(getApplicationContext(), FindPWActivity.class);
                startActivity(findpwIntent);
            }
        });

        //로그인하기
        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //로그인 버튼 눌렀을 시 서버와 통신
                final Request request = RetrofitAPI.getApICall();

                final String email = inputId.getText().toString();
                final String password = inputPw.getText().toString();

                if (email.length() == 0) {
                    inputId.setError("이메일을 입력해주세요.");
                    inputId.requestFocus();
                    return;
                } else if (password.isEmpty()) {
                    inputPw.setError("비밀번호를 입력해주세요.");
                    return;
                }

                Call<JWTToken> jwtTokenCall = request.userlogin(email, password);

                jwtTokenCall.enqueue(new Callback<JWTToken>() {
                    @Override
                    public void onResponse(Call<JWTToken> call, Response<JWTToken> response) {
                        if (response.isSuccessful()) {
                            JWTToken jwtToken = response.body();
                            tokenManager.createSession(jwtToken.getToken().toString());

                            if (response.code() == 200) {
                                Intent loginIntent = new Intent(getApplicationContext(), MainBoardActivity.class);
                                loginIntent.putExtra("token", jwtToken.getToken().toString());
                                loginIntent.putExtra("userEmail", email.toString());
                                startActivity(loginIntent);
                                finish();
                            }
                        } else {
                            if (response.code() == 500) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                                dialog = builder
                                        .setMessage("이메일 혹은 비밀번호가 틀렸습니다.")
                                        .setNegativeButton("확인", null)
                                        .create();
                                dialog.show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<JWTToken> call, Throwable t) {
                        showToast("네트워크 오류");

                        Log.d("Login", "onResponse");

                    }
                });

            }
        });


    }

    void showToast(String msg) {
        Toast.makeText(this, "" + msg, Toast.LENGTH_LONG).show();
    }
}
