package com.example.hyehy.helpme;

public class MonthlyUsageList {

    private String monthlyUsageDate;
    private String monthlyUsageName;
    private String monthlyUsageCost;

    public MonthlyUsageList(String monthlyUsageName, String monthlyUsageCost, String monthlyUsageDate) {
        this.monthlyUsageDate = monthlyUsageDate;
        this.monthlyUsageName = monthlyUsageName;
        this.monthlyUsageCost = monthlyUsageCost;
    }


    public String getMonthlyUsageDate() {
        return monthlyUsageDate;
    }

    public void setMonthlyUsageDate(String monthlyUsageDate) {
        this.monthlyUsageDate = monthlyUsageDate;
    }

    public String getMonthlyUsageName() {
        return monthlyUsageName;
    }

    public void setMonthlyUsageName(String monthlyUsageName) {
        this.monthlyUsageName = monthlyUsageName;
    }

    public String getMonthlyUsageCost() {
        return monthlyUsageCost;
    }

    public void setMonthlyUsageCost(String monthlyUsageCost) {
        this.monthlyUsageCost = monthlyUsageCost;
    }


}
