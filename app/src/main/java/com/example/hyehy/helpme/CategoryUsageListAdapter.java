package com.example.hyehy.helpme;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class CategoryUsageListAdapter extends BaseAdapter {

    Context context;
    ArrayList<CategoryUsageList> list_categoryUsage;

    ImageView imageViewMain;
    TextView tvCategoryName;
    TextView tvCategoryTotal;

    public CategoryUsageListAdapter(Context context, ArrayList<CategoryUsageList> list_categoryUsage) {
        this.context = context;
        this.list_categoryUsage = list_categoryUsage;
    }


    //리스트뷰가 몇개의 아이템을 가지고 있는지 알려줌
    @Override
    public int getCount() {
        return this.list_categoryUsage.size();
    }

    //현재 어떤 아이템인지 알려줌
    @Override
    public Object getItem(int position) {
        return list_categoryUsage.get(position);
    }

    //현재 어떤 포지션인지 알려줌
    @Override
    public long getItemId(int position) {
        return position;
    }

    //리스트뷰에서 아이템과 xml을 연결해서 화면에 표시해줌
    //convertView라는 파라미터를 메소드에서 줌. 이부분에 xml을 불러와야 함
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(context).inflate(R.layout.item_category_usage, null);
        imageViewMain = convertView.findViewById(R.id.imageView_main);
        tvCategoryName = convertView.findViewById(R.id.tv_category_name);
        tvCategoryTotal = convertView.findViewById(R.id.tv_category_total);

        imageViewMain.setImageResource(list_categoryUsage.get(position).getImageMain());
        tvCategoryName.setText(list_categoryUsage.get(position).getCategoryName());
        tvCategoryTotal.setText(list_categoryUsage.get(position).getCategoryTotalUsage());


        return convertView;
    }
}