package com.example.hyehy.helpme;

public class CategoryUsageList {

    private int imageMain;
    private String categoryName;
    private String categoryTotalUsage;

    public CategoryUsageList(int imageMain, String categoryName, String categoryTotalUsage) {
        this.imageMain = imageMain;
        this.categoryName = categoryName;
        this.categoryTotalUsage = categoryTotalUsage;
    }

    public int getImageMain() {
        return imageMain;
    }

    public void setImageMain(int imageMain) {
        this.imageMain = imageMain;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryTotalUsage() {
        return categoryTotalUsage;
    }

    public void setCategoryTotalUsage(String categoryTotalUsage) {
        this.categoryTotalUsage = categoryTotalUsage;
    }
}
