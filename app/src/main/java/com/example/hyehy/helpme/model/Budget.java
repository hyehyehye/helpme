package com.example.hyehy.helpme.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Budget {
    @SerializedName("budget") // JSON에서 표시되는 방법
    @Expose

    public int budget;

    public int getBudget() {
        return budget;
    }
}
