package com.example.hyehy.helpme;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.hyehy.helpme.fragment.DailyUsageFragment;
import com.example.hyehy.helpme.model.PurchaseList;
import com.example.hyehy.helpme.remote.Request;
import com.example.hyehy.helpme.remote.RetrofitAPI;
import com.example.hyehy.helpme.tokenmanager.TokenManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HandinputActivity extends AppCompatActivity {
    private TokenManager tokenManager;

    EditText inputProductName;
    EditText inputProductCost;
    EditText inputProductDate;
    Button btnSubmit;
    Button btnCancel;

    String productName;
    String productCost;
    String productDate;
    int productCostInt;

    String token;

    private AlertDialog dialog1;

    protected void onStop() {
        super.onStop();
        if (dialog1 != null) {
            dialog1.dismiss();
            dialog1 = null;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_handinput_usage);

        inputProductName = findViewById(R.id.input_product_name);
        inputProductCost = findViewById(R.id.input_product_cost);
        inputProductDate = findViewById(R.id.input_product_date);

        btnSubmit = findViewById(R.id.btn_handinput_submit);
        btnCancel = findViewById(R.id.btn_handinput_cancel);

        productName = inputProductName.getText().toString();
        productCost = inputProductCost.getText().toString();
        productDate = inputProductDate.getText().toString();


        tokenManager = new TokenManager(getApplicationContext());
        token = tokenManager.getto();


        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Request request = RetrofitAPI.getApICall();
                Call<PurchaseList> purcall = request.InputPurchase(token, productName, productCostInt, productDate);
                purcall.enqueue(new Callback<PurchaseList>() {
                    @Override
                    public void onResponse(Call<PurchaseList> call, Response<PurchaseList> response) {
                        if (response.code() == 200) {
                            PurchaseList purlist = response.body();
                            AlertDialog.Builder builder = new AlertDialog.Builder(HandinputActivity.this);
                            dialog1 = builder.setMessage("지출내역 입력이 완료되었습니다.").setNegativeButton("확인", new DialogInterface.OnClickListener() {
                                public void onClick(
                                        DialogInterface dialog, int id) {
                                    Intent Ok = new Intent(getApplicationContext(), DailyUsageFragment.class);
                                    finish();
                                }
                            }).create();
                            dialog1.show();
                        } else if (response.code() == 500) {
                            showToast("전송 실패. 서버에러");
                        }
                    }


                    @Override
                    public void onFailure(Call<PurchaseList> call, Throwable t) {
                        showToast("네트워크 연결 상태를 확인해주세요.");

                    }
                });


            }
        });


        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    void showToast(String msg) {
        Toast.makeText(this, "" + msg, Toast.LENGTH_LONG).show();
    }

}