package com.example.hyehy.helpme.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.hyehy.helpme.CategoryShareList;
import com.example.hyehy.helpme.CategoryShareListAdapter;

import com.example.hyehy.helpme.CategoryUsageList;
import com.example.hyehy.helpme.CategoryUsageListAdapter;

import com.example.hyehy.helpme.R;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;

public class CategoryFragment extends Fragment {
    PieChart pieChart;
    ListView listViewRank;
    Button btnLayoutChange;
    String toastText;
    LinearLayout layoutCategory;
    LinearLayout layoutUsage;

    ListView listViewCategoryUsage;
    CategoryUsageListAdapter categoryUsageListAdapter;
    ArrayList<CategoryUsageList> list_categoryUsageArrayList;

    ListView listviewCategoryShare;
    CategoryShareListAdapter categoryShareListAdapter;
    ArrayList<CategoryShareList> list_categoryShareArrayList;

    public static int fasionPer;
    public static int cosmeticPer;
    public static int digitalPer;
    public static int interiorPer;
    public static int kidsPer;
    public static int foodPer;
    public static int sportsPer;
    public static int lifePer;
    public static int culturePer;

    public static CategoryFragment INSTANCE = null;
    static CoordinatorLayout mContainer = null;


    public static CategoryFragment getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new CategoryFragment();
        }
        return INSTANCE;
    }

    @Override
    public CoordinatorLayout onCreateView(LayoutInflater inflater, ViewGroup container,
                                          Bundle savedInstanceState) {
        if (mContainer == null) {
            mContainer = (CoordinatorLayout) inflater.inflate(R.layout.fragment_category, null);
        }

        pieChart = mContainer.findViewById(R.id.piechart);
        listViewRank = mContainer.findViewById(R.id.listView_category_rank);
        btnLayoutChange = mContainer.findViewById(R.id.btn_layout_change);

        layoutCategory = mContainer.findViewById(R.id.layout_category);
        layoutUsage = mContainer.findViewById(R.id.layout_category_usage);

        layoutCategory.setVisibility(View.VISIBLE);
        layoutUsage.setVisibility(View.INVISIBLE);

        //레이아웃 전환 온클릭 설정
        btnLayoutChange.setOnClickListener((View v1) -> {
            changeView();
        });

        //이 값은 후에 서버에서 받아오는거로 고쳐야함ㅠㅠ
        fasionPer = 21;
        cosmeticPer = 17;
        digitalPer = 15;
        interiorPer = 14;
        kidsPer = 10;
        foodPer = 8;
        sportsPer = 6;
        lifePer = 5;
        culturePer = 4;


        //파이차트
        pieChart.setUsePercentValues(true);
        pieChart.getDescription().setEnabled(true);
        pieChart.setExtraOffsets(5, 10, 5, 5);
        pieChart.setDragDecelerationFrictionCoef(0.95f);

        pieChart.setHoleRadius(35f);
        pieChart.setHoleColor(Color.WHITE);
        pieChart.setCenterText("카테고리별 점유율");
        pieChart.setCenterTextSize(20f);
        pieChart.setTransparentCircleRadius(0);
        pieChart.getLegend().setEnabled(false);
        pieChart.animateY(1100, Easing.EasingOption.EaseInOutCubic); //애니메이션

        Description description = new Description();
        description.setText(" "); //라벨
        description.setTextSize(15);
        pieChart.setDescription(description);

        addDataSet(pieChart); //파이차트에 값 넣는 부분


        //파이차트 밑에 리스트뷰
        listviewCategoryShare = mContainer.findViewById(R.id.listView_category_rank);
        listviewCategoryShare.setAdapter(generateAdapterShare());


        //레이아웃 전환해서
        //한달간 카테고리별로 사용한 리스트뷰
        listViewCategoryUsage = mContainer.findViewById(R.id.listView_category_usage);
        listViewCategoryUsage.setAdapter(generateAdapterUsage());

        return mContainer;
    }


    //카테고리별 점유율 리스트뷰 추가
    private ListAdapter generateAdapterShare() {
        list_categoryShareArrayList = new ArrayList<CategoryShareList>();

        list_categoryShareArrayList.add(
                new CategoryShareList("1위", "패션", String.valueOf(fasionPer))
        );

        list_categoryShareArrayList.add(
                new CategoryShareList("2위", "화장품", String.valueOf(cosmeticPer))
        );

        list_categoryShareArrayList.add(
                new CategoryShareList("3위", "디지털/가전", String.valueOf(digitalPer))
        );

        list_categoryShareArrayList.add(
                new CategoryShareList("4위", "가구/인테리어", String.valueOf(interiorPer))
        );

        list_categoryShareArrayList.add(
                new CategoryShareList("5위", "출산/육아", String.valueOf(kidsPer))
        );

        list_categoryShareArrayList.add(
                new CategoryShareList("6위", "식품", String.valueOf(foodPer))
        );

        list_categoryShareArrayList.add(
                new CategoryShareList("7위", "스포츠", String.valueOf(sportsPer))
        );

        list_categoryShareArrayList.add(
                new CategoryShareList("8위", "생활", String.valueOf(lifePer))
        );

        list_categoryShareArrayList.add(
                new CategoryShareList("9위", "문화", String.valueOf(culturePer))
        );
        return new CategoryShareListAdapter(getActivity(), list_categoryShareArrayList);
    }

    //카테고리별 사용금액 리스트뷰에 추가ㅏ
    private ListAdapter generateAdapterUsage() {
        list_categoryUsageArrayList = new ArrayList<CategoryUsageList>();

        list_categoryUsageArrayList.add(
                new CategoryUsageList(R.drawable.food_image, "식품", "100000원")
        );

        list_categoryUsageArrayList.add(
                new CategoryUsageList(R.drawable.fashion_image, "패션", "1500000원")
        );

        list_categoryUsageArrayList.add(
                new CategoryUsageList(R.drawable.sports_image, "스포츠", "2100000원")
        );

        list_categoryUsageArrayList.add(
                new CategoryUsageList(R.drawable.kids_image, "출산/유아", "160000원")
        );

        list_categoryUsageArrayList.add(
                new CategoryUsageList(R.drawable.heart, "여행", "1800000원")
        );

        list_categoryUsageArrayList.add(
                new CategoryUsageList(R.drawable.heart, "아악6", "66666원")
        );
        return new CategoryUsageListAdapter(getActivity(), list_categoryUsageArrayList);
    }


    //파이차트에 데이터 넣기
    private void addDataSet(PieChart pieChart) {
        ArrayList<PieEntry> yValues = new ArrayList<PieEntry>();
        if (foodPer != 0) {
            yValues.add(new PieEntry(foodPer, "식품"));
        }
        if (fasionPer != 0) {
            yValues.add(new PieEntry(fasionPer, "패션"));
        }
        if (cosmeticPer != 0) {
            yValues.add(new PieEntry(cosmeticPer, "화장품/미용"));
        }
        if (digitalPer != 0) {
            yValues.add(new PieEntry(digitalPer, "디지털/가전"));
        }
        if (interiorPer != 0) {
            yValues.add(new PieEntry(interiorPer, "가구/인테리어"));
        }
        if (kidsPer != 0) {
            yValues.add(new PieEntry(kidsPer, "출산/육아"));
        }
        if (sportsPer != 0) {
            yValues.add(new PieEntry(sportsPer, "스포츠/레저"));
        }
        if (lifePer != 0) {
            yValues.add(new PieEntry(lifePer, "생활/건강"));
        }
        if (culturePer != 0) {
            yValues.add(new PieEntry(culturePer, "여행/문화"));
        }

        PieDataSet dataSet = new PieDataSet(yValues, "분류별 점유율");
        dataSet.setSliceSpace(1f);
        dataSet.setSelectionShift(5f);
        dataSet.setColors(ColorTemplate.COLORFUL_COLORS);

        PieData data = new PieData((dataSet));
        data.setValueTextSize(12f);
        data.setValueTextColor(Color.BLACK);

        pieChart.setData(data);
        pieChart.invalidate(); // refresh
    }

    //레이아웃 전환하는거
    public void changeView() {
        //만약 파이그래프 있는 화면이 보인다면
        if (layoutCategory.getVisibility() == View.VISIBLE) {
            layoutCategory.setVisibility(View.INVISIBLE);
            layoutUsage.setVisibility(View.VISIBLE);
            btnLayoutChange.setText("분류별 점유율 보기");

        } else {
            layoutCategory.setVisibility(View.VISIBLE);
            layoutUsage.setVisibility(View.INVISIBLE);
            btnLayoutChange.setText("분류별 지출 금액 보기");

        }

    }


}
