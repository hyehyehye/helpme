package com.example.hyehy.helpme;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class MonthlyUsageListAdapter extends BaseAdapter {

    Context context;
    TextView tvMonthlyUsageDate;
//    TextView tvMonthlyUsageName;
    TextView tvMonthlyUsageCost;
    ArrayList<MonthlyUsageList> list_monthlyUsage;

    public MonthlyUsageListAdapter(Context context, ArrayList<MonthlyUsageList> list_monthlyUsage) {
        this.context = context;
        this.list_monthlyUsage = list_monthlyUsage;
    }



    @Override
    public int getCount() {
        return this.list_monthlyUsage.size();
    }

    @Override
    public Object getItem(int position) {
        return this.list_monthlyUsage.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        convertView = LayoutInflater.from(context).inflate(R.layout.item_monthly_usage, null);
        tvMonthlyUsageDate = convertView.findViewById(R.id.tv_month_date);
//        tvMonthlyUsageName = convertView.findViewById(R.id.tv_month_name);
        tvMonthlyUsageCost = convertView.findViewById(R.id.tv_month_cost);

        tvMonthlyUsageDate.setText(list_monthlyUsage.get(position).getMonthlyUsageDate());
//        tvMonthlyUsageName.setText(list_monthlyUsage.get(position).getMonthlyUsageName());
        tvMonthlyUsageCost.setText(list_monthlyUsage.get(position).getMonthlyUsageCost());

        return convertView;
    }
}
