package com.example.hyehy.helpme;

public class CategoryShareList {

    private String cateShareRank;
    private String cateShareName;
    private String cateSharePercent;


    public CategoryShareList(String cateShareRank, String cateShareName, String cateSharePercent) {
        this.cateShareRank = cateShareRank;
        this.cateShareName = cateShareName;
        this.cateSharePercent = cateSharePercent;
    }


    public String getCateShareRank() {
        return cateShareRank;
    }

    public void setCateShareRank(String cateShareRank) {
        this.cateShareRank = cateShareRank;
    }

    public String getCateShareName() {
        return cateShareName;
    }

    public void setCateShareName(String cateShareName) {
        this.cateShareName = cateShareName;
    }

    public String getCateSharePercent() {
        return cateSharePercent;
    }

    public void setCateSharePercent(String cateSharePercent) {
        this.cateSharePercent = cateSharePercent;
    }
}
