package com.example.hyehy.helpme.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hyehy.helpme.R;
import com.example.hyehy.helpme.model.Budget;
import com.example.hyehy.helpme.model.BudgetStatus;
import com.example.hyehy.helpme.remote.Request;
import com.example.hyehy.helpme.remote.RetrofitAPI;
import com.example.hyehy.helpme.tokenmanager.TokenManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BudgetFragment extends Fragment {
    public static BudgetFragment INSTANCE = null;

    TextView tvBudget; //이번달 예산
    TextView tvTotalUsage; //오늘까지 총 지출
    TextView tvLeftBudget; //남은 예산
    TextView tvLeftSpare; //하루에 쓸 수 있는 돈
    Button btnInputBudget;

    private TokenManager tokenManager;



    public static BudgetFragment getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new BudgetFragment();
        }
        return INSTANCE;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_budget, container, false);
        return view;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        tvBudget = getView().findViewById(R.id.text_budget);
        tvTotalUsage = getView().findViewById(R.id.text_total_usage);
        tvLeftBudget = getView().findViewById(R.id.text_left_budget);
        tvLeftSpare = getView().findViewById(R.id.text_left_spare);
        btnInputBudget = getView().findViewById(R.id.btn_input_budget);

        tokenManager = new TokenManager(getActivity().getApplicationContext());


        String token = tokenManager.getto();
        final Request request = RetrofitAPI.getApICall();
        Call<Budget> budgetCall = request.requestBudget(token);
        budgetCall.enqueue(new Callback<Budget>() {
            @Override
            public void onResponse(Call<Budget> call, Response<Budget> response) {
                if(response.code()==200){
                    Budget budgetinfo=response.body();
                    tvBudget.setText(""+budgetinfo.budget+"원");
                }
                else if(response.code()==500)
                    showToast("서버 오류");
            }
            @Override
            public void onFailure(Call<Budget> call, Throwable t) {
                showToast("네트워크 상태를 확인해주세요");
            }
        });


        //남은 예산 및 하루에 쓸 금액 표시
        Call<BudgetStatus> budgetCall2 = request.requestBudgetStatus(token);
        budgetCall2.enqueue(new Callback<BudgetStatus>() {
            @Override
            public void onResponse(Call<BudgetStatus> call, Response<BudgetStatus> response) {
                if(response.code()==200){
                    BudgetStatus budgetinfo=response.body();
                    if(budgetinfo.over==false) {
                        tvBudget.setText("" + budgetinfo.budget+"원");
                        tvBudget.setTextColor(getResources().getColor(R.color.colorAccent));
                        tvTotalUsage.setText(budgetinfo.month_price+"원");
                        tvLeftBudget.setText(budgetinfo.diff_price+"원");
                        tvLeftSpare.setText(budgetinfo.rest_price+"원");
                    }
                    else if(budgetinfo.over==true) {
                        tvBudget.setText("" + budgetinfo.budget+"원");
                        tvBudget.setTextColor(Color.RED);
                        tvTotalUsage.setText(budgetinfo.month_price);
                        tvLeftBudget.setText(budgetinfo.diff_price+"원 초과!");
                        tvLeftSpare.setText("예산 초과!");
                    }
                }
                else if(response.code()==500)
                    showToast("서버 오류");
            }

            @Override
            public void onFailure(Call<BudgetStatus> call, Throwable t) {
                showToast("네트워크 상태를 확인해주세요");
            }
        });


        //예산 입력 버튼을 눌렀을 때
        btnInputBudget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final EditText edittext = new EditText(getActivity());
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("예산설정");
                builder.setMessage("예산을 입력해주세요");
                builder.setView(edittext);
                builder.setPositiveButton("입력",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                if(edittext.getText().length()==0)
                                    showToast("금액을 입력해주세요.");
                                else {
                                    Integer budget=new Integer(edittext.getText().toString());
                                    Call<Budget> budgetcall=request.postBudget(token,budget.intValue());
                                    budgetcall.enqueue(new Callback<Budget>() {
                                        @Override
                                        public void onResponse(Call<Budget> call, Response<Budget> response) {
                                            if(response.code()==200) {
                                                showToast("예산설정이 완료되었습니다.");
                                                tvBudget.setText(budget.toString()+"원");
                                                dialog.dismiss();
                                            }
                                            else
                                                showToast("서버에러");
                                        }
                                        @Override
                                        public void onFailure(Call<Budget> call, Throwable t) {
                                            showToast("네트워크 연결 상태를 확인해주세요");
                                        }
                                    });

                                }
                            }
                        });
                builder.setNegativeButton("취소",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                builder.show();

            }

        });

    }


    void showToast(String msg)
    {
        Toast.makeText(getActivity(),""+msg, Toast.LENGTH_LONG).show();
    }

}