package com.example.hyehy.helpme.fragment;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.example.hyehy.helpme.MonthlyUsageList;
import com.example.hyehy.helpme.MonthlyUsageListAdapter;
import com.example.hyehy.helpme.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class MonthlyUsageFragment extends Fragment {
    public static MonthlyUsageFragment INSTANCE = null;
    static CoordinatorLayout mContainer = null;

    TextView tvMonth;
    TextView tvPrevMonth;
    TextView tvNextMonth;
    String thisMonthString;
    String thisDayString;
    int thisDayInt;
    int thisMonthInt;
    String thisDate;
    private static final String PATTERN_DATE_MONTH = "M";
    private static final String PATTERN_DATE_DAY = "dd";

    ListView listviewMonthlyUsage;
    MonthlyUsageListAdapter monthlyUsageListAdapter;
    ArrayList<MonthlyUsageList> list_monthlyUsageArrayList;

    public static MonthlyUsageFragment getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new MonthlyUsageFragment();
        }
        return INSTANCE;
    }

    @Override
    public CoordinatorLayout onCreateView(LayoutInflater inflater, ViewGroup container,
                                          Bundle savedInstanceState) {
        if (mContainer == null) {
            mContainer = (CoordinatorLayout) inflater.inflate(R.layout.fragment_monthly_usage, null);
        }

        tvMonth = mContainer.findViewById(R.id.tv_month);
        tvPrevMonth = mContainer.findViewById(R.id.tv_prev_month);
        tvNextMonth = mContainer.findViewById(R.id.tv_next_month);

        //현재 월,일 설정 (String, int)
        thisDayString = getDataString(PATTERN_DATE_MONTH);
        thisMonthString = getDataString(PATTERN_DATE_DAY);

        thisDayInt = Integer.parseInt(thisDayString);
        thisMonthInt = Integer.parseInt(thisMonthString);

        thisDate = thisMonthString + "/" + thisDayString;

        tvMonth.setText(thisMonthString + "월");

        //한달 전
        tvPrevMonth.setOnClickListener(v1 -> {
            calcMonthNext();
        });

        //한달 후
        tvNextMonth.setOnClickListener(v1 -> {
            calcMonthPrev();
        });

        //하단 리스트뷰
        listviewMonthlyUsage = mContainer.findViewById(R.id.listView_monthly_usage);
        list_monthlyUsageArrayList = new ArrayList<MonthlyUsageList>();
        listviewMonthlyUsage.setAdapter(generateAdapter());


        return mContainer;
    }


    private String getDataString(String pattern) {
        DateFormat dateFormat = new SimpleDateFormat(PATTERN_DATE_MONTH);
        Date nowMonth = new Date();
        return dateFormat.format(nowMonth);
    }

    //리스트뷰에 실제로 들어가는 내용
    private MonthlyUsageListAdapter generateAdapter() {
        list_monthlyUsageArrayList.add(new MonthlyUsageList("까르보네피자", "10000", "9/1"));
        list_monthlyUsageArrayList.add(new MonthlyUsageList("불고기피자", "15000", "9/2"));
        list_monthlyUsageArrayList.add(new MonthlyUsageList("포테이토피자", "20000", "9/3"));
        list_monthlyUsageArrayList.add(new MonthlyUsageList("고구마피자", "9000", "9/4"));
        list_monthlyUsageArrayList.add(new MonthlyUsageList("슈프림피자", "13000", "9/5"));

        return new MonthlyUsageListAdapter(getActivity(), list_monthlyUsageArrayList);
    }


    public void calcMonthNext() {
        thisMonthInt = Integer.parseInt(thisMonthString);
        thisMonthInt -= 1;

        if (thisMonthInt < 1) {
            thisMonthInt = 12;
        }
        setDateInfo();
    }


    public void calcMonthPrev() {
        thisMonthInt = Integer.parseInt(thisMonthString);
        thisMonthInt += 1;

        if (thisMonthInt > 12) {
            thisMonthInt = 1;
        }
        setDateInfo();
    }

    private void setDateInfo() {
        thisMonthString = String.valueOf(thisMonthInt);
        tvMonth.setText(thisMonthString + "월");
    }

}
