package com.example.hyehy.helpme;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.hyehy.helpme.R;
import com.example.hyehy.helpme.model.Message;
import com.example.hyehy.helpme.remote.Request;
import com.example.hyehy.helpme.remote.RetrofitAPI;
import com.example.hyehy.helpme.tokenmanager.TokenManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InquiryActivity extends AppCompatActivity {

    EditText inputInquiry;
    Button btnBack;
    Button btnInquirySubmit;
    private TokenManager tokenManager;
    private AlertDialog dialog1;
    String content;

    protected void onStop() {
        super.onStop();
        if (dialog1 != null) {
            dialog1.dismiss();
            dialog1 = null;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_inquiry);

        tokenManager = new TokenManager(getApplicationContext());

        inputInquiry = findViewById(R.id.input_inquiry);
        btnBack = findViewById(R.id.button_back);
        btnInquirySubmit = findViewById(R.id.button_submit);

        String token = tokenManager.getto();
        content = inputInquiry.getText().toString();


        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        btnInquirySubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //제출 버튼 눌렀을 때
                if (content.equals(null))
                    showToast("내용을 입력해주세요");
                else {
                    final Request request = RetrofitAPI.getApICall();
                    Call<Message> Questioncall = request.questionReq(token, content);
                    Questioncall.enqueue(new Callback<Message>() {
                        @Override
                        public void onResponse(Call<Message> call, Response<Message> response) {
                            Message message = response.body();
                            AlertDialog.Builder builder = new AlertDialog.Builder(InquiryActivity.this);
                            dialog1 = builder.setMessage("관리자에게 전송되었습니다.").setNegativeButton("확인", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    Intent Ok = new Intent(getApplicationContext(), MainActivity.class);
                                    startActivity(Ok);
                                    finish();
                                }
                            }).create();
                            dialog1.show();
                        }

                        @Override
                        public void onFailure(Call<Message> call, Throwable t) {
                            showToast("네트워크 통신 상태를 확인하세요.");
                        }

                    });
                }
            }
        });
    }


    void showToast(String msg) {
        Toast.makeText(this, "" + msg, Toast.LENGTH_LONG).show();
    }


}
