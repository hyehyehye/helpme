package com.example.hyehy.helpme.common;


import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

public class ExtendedActivity extends AppCompatActivity {
  public void toast(String msg) {
    Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
  }
}
