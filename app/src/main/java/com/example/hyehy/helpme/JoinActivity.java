package com.example.hyehy.helpme;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hyehy.helpme.model.Message;
import com.example.hyehy.helpme.remote.Request;
import com.example.hyehy.helpme.remote.RetrofitAPI;

import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class JoinActivity extends AppCompatActivity {
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    private AlertDialog dialog1;
    public String check;

    EditText inputJoinName;
    EditText inputJoinEmail;
    EditText inputJoinPhone;
    EditText inputJoinPw;
    EditText inputCoupangID;
    EditText inputCoupangPW;
    Button buttonSignUp;
    Button btnBacktoLogin;


    @Override
    protected void onStop() {
        super.onStop();
        if (dialog1 != null) {
            dialog1.dismiss();
            dialog1 = null;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_join);

        inputJoinName = findViewById(R.id.input_join_name);
        inputJoinEmail = findViewById(R.id.input_join_email);
        inputJoinPhone = findViewById(R.id.input_join_phone);
        inputJoinPw = findViewById(R.id.input_join_pw);
        inputCoupangID = findViewById(R.id.input_coupang_id);
        inputCoupangPW = findViewById(R.id.input_coupang_pw);
        buttonSignUp = findViewById(R.id.button_sign_up);
        btnBacktoLogin = findViewById(R.id.button_back_to_login);


        btnBacktoLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent backIntent = new Intent(getApplicationContext(),LoginActivity.class);
                backIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(backIntent);
            }
        });

        buttonSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String joinName = inputJoinName.getText().toString();
                final String joinEmail = inputJoinEmail.getText().toString();
                final String joinPhone = inputJoinPhone.getText().toString();
                final String joinPW = inputJoinPw.getText().toString();
                final String coupangID = inputCoupangID.getText().toString();
                final String coupangPW = inputCoupangPW.getText().toString();

                if (joinName.length() == 0) {
                    inputJoinName.setError("이름을 입력해주세요.");
                    inputJoinName.requestFocus();
                    return;

                } else if (joinEmail.isEmpty()) {
                    inputJoinEmail.setError("이메일을 입력해주세요.");
                    return;

                } else if (joinPhone.isEmpty()) {
                    inputJoinPhone.setError("핸드폰 번호를 입력해주세요.");
                    return;

                } else if (joinPW.isEmpty()) {
                    inputJoinPw.setError("비밀번호를 입력해주세요.");
                    return;

                } else if (coupangID.isEmpty()) {
                    inputCoupangID.setError("쿠팡 아이디를 입력해주세요.");
                    return;

                } else if (coupangPW.isEmpty()) {
                    inputCoupangPW.setError("쿠팡 비밀번호를 입력해주세요.");
                    return;
                }

                final Request request = RetrofitAPI.getApICall();
                Call<Message> regcall = request.registerUser(joinName, joinEmail, joinPhone, joinPW, coupangID, coupangPW);

                regcall.enqueue(new Callback<Message>() {
                    @Override
                    public void onResponse(Call<Message> call, Response<Message> response) {
                        if (response.isSuccessful()) {
                            Message message = response.body();
                            if (response.code() == 200) {
                                showToast("" + message.getMessage().toString());
                                finish();
                            } else if (response.code() == 501) {
                                inputJoinEmail.setError("중복된 아이디 입니다.");
                                AlertDialog.Builder builder = new AlertDialog.Builder(JoinActivity.this);
                                dialog1 = builder.setMessage("중복된 아이디 입니다.").setNegativeButton("확인", null).create();
                                dialog1.show();

                            } else if (response.code() == 502) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(JoinActivity.this);
                                dialog1 = builder.setMessage("값을 모두 입력해주세요.").setNegativeButton("확인", null).create();
                                dialog1.show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<Message> call, Throwable t) {
                        showToast("네트워크 상태를 확인해주세요");
                        Log.d("join", "onResponse");

                    }
                });
            }
        });

    }


    void showToast(String msg) {
        Toast.makeText(this, "" + msg, Toast.LENGTH_LONG).show();
    }

}
