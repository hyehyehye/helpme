package com.example.hyehy.helpme;

import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.hyehy.helpme.R;
import com.example.hyehy.helpme.model.Message;
import com.example.hyehy.helpme.remote.Request;
import com.example.hyehy.helpme.remote.RetrofitAPI;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FindPWActivity extends AppCompatActivity {

    EditText inputName;
    EditText inputPhone;
    EditText inputEmail;
    Button buttonFindID;
    Button btnBack;
    private AlertDialog dialog1;

    @Override
    protected void onStop() {
        super.onStop();
        if(dialog1 != null){
            dialog1.dismiss();
            dialog1=null;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_find_pw);

        inputName = findViewById(R.id.input_find_name);
        inputEmail = findViewById(R.id.input_find_email);
        inputPhone = findViewById(R.id.input_find_phone_number);
        buttonFindID = findViewById(R.id.button_find_pw);
        btnBack = findViewById(R.id.button_back);


        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        buttonFindID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Request request = RetrofitAPI.getApICall();
                final String name1 =inputName.getText().toString();
                final String email1 = inputEmail.getText().toString();
                final String phone1 = inputPhone.getText().toString();
                if(inputName.length() ==0){
                    inputName.setError("이름을 입력하세요.");
                    inputName.requestFocus();
                    return;
                }else if(email1.isEmpty()){
                    inputEmail.setError("이메일을 입력하세요.");
                    return;
                } else if(phone1.isEmpty()){
                    inputPhone.setError("핸드폰 번호를 입력하세요.");
                    return;

                }
                Call<Message> findpwcall = request.findUserpw(name1,email1,phone1);

                findpwcall.enqueue(new Callback<Message>() {
                    @Override
                    public void onResponse(Call<Message> call, Response<Message> response) {
                        if (response.isSuccessful()) { //성공인 경우
                            Message message = response.body();
                            if (response.code() == 200) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(FindPWActivity.this);
                                dialog1 = builder.setMessage("" + message.getMessage().toString()).setNegativeButton("확인", null).create();
                                dialog1.show();
                            }
                        } else { //실패인 경우
                            if (response.code() == 500) {
                                showToast("입력하신 정보가 일치하지 않습니다.");
                            }
                            if(response.code()==501)
                            {
                                showToast("서버 오류. 잠시 후 다시 시도해주세요.");
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<Message> call, Throwable t) {
                        showToast("연결에 실패했습니다. 잠시 후 다시 시도해주세요.");
                    }
                });

            }
        });
    }
    void showToast(String msg)
    {
        Toast.makeText(this, ""+msg, Toast.LENGTH_LONG).show();
    }
}
