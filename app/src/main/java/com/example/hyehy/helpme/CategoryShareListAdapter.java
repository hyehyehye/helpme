package com.example.hyehy.helpme;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class CategoryShareListAdapter extends BaseAdapter {

    Context context;
    TextView tvCategoryRank;
    TextView tvCategoryShareName;
    TextView tvCategoryPercent;

    ArrayList<CategoryShareList> list_categoryshare;

    public CategoryShareListAdapter(Context context, ArrayList<CategoryShareList> list_categoryshare) {
        this.context = context;
        this.list_categoryshare = list_categoryshare;
    }


    @Override
    public int getCount() {
        return this.list_categoryshare.size();
    }

    @Override
    public Object getItem(int position) {
        return this.list_categoryshare.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        convertView = LayoutInflater.from(context).inflate(R.layout.item_category_share, null);
        tvCategoryRank = convertView.findViewById(R.id.tv_category_rank);
        tvCategoryShareName = convertView.findViewById(R.id.tv_category_share_name);
        tvCategoryPercent = convertView.findViewById(R.id.tv_category_percent);

        tvCategoryRank.setText(list_categoryshare.get(position).getCateShareRank());
        tvCategoryShareName.setText(list_categoryshare.get(position).getCateShareName());
        tvCategoryPercent.setText(list_categoryshare.get(position).getCateSharePercent());


        return convertView;
    }
}
