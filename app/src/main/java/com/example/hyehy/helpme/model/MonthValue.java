package com.example.hyehy.helpme.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class MonthValue {
    @SerializedName("month_price")
    @Expose
    public int month_price;

    @SerializedName("day_price")
    @Expose
    public int day_price;

    @SerializedName("month_list")
    @Expose
    ArrayList<data> month_list = new ArrayList<>();

    public ArrayList<data> getMonth_list() {
        return month_list;
    }

    public class dayPrice{
        @SerializedName("1")
        @Expose
        public String day_1;
        @SerializedName("2")
        @Expose
        public String day_2;
        @SerializedName("3")
        @Expose
        public String day_3;
        @SerializedName("4")
        @Expose
        public String day_4;
        @SerializedName("5")
        @Expose
        public String day_5;
        @SerializedName("6")
        @Expose
        public String day_6;
        @SerializedName("7")
        @Expose
        public String day_7;
        @SerializedName("8")
        @Expose
        public String day_8;
        @SerializedName("9")
        @Expose
        public String day_9;
        @SerializedName("10")
        @Expose
        public String day_10;
        @SerializedName("11")
        @Expose
        public String day_11;
        @SerializedName("12")
        @Expose
        public String day_12;
        @SerializedName("13")
        @Expose
        public String day_13;
        @SerializedName("14")
        @Expose
        public String day_14;
        @SerializedName("15")
        @Expose
        public String day_15;
        @SerializedName("16")
        @Expose
        public String day_16;
        @SerializedName("17")
        @Expose
        public String day_17;
        @SerializedName("18")
        @Expose
        public String day_18;
        @SerializedName("19")
        @Expose
        public String day_19;
        @SerializedName("20")
        @Expose
        public String day_20;
        @SerializedName("21")
        @Expose
        public String day_21;
        @SerializedName("22")
        @Expose
        public String day_22;
        @SerializedName("23")
        @Expose
        public String day_23;
        @SerializedName("24")
        @Expose
        public String day_24;
        @SerializedName("25")
        @Expose
        public String day_25;
        @SerializedName("26")
        @Expose
        public String day_26;
        @SerializedName("27")
        @Expose
        public String day_27;
        @SerializedName("28")
        @Expose
        public String day_28;
        @SerializedName("29")
        @Expose
        public String day_29;
        @SerializedName("30")
        @Expose
        public String day_30;
        @SerializedName("31")
        @Expose
        public String day_31;
    }

    public static class data{
        @SerializedName("id")
        @Expose
        public Integer id;
        @SerializedName("email")
        @Expose
        public Integer email;
        @SerializedName("item_name")
        @Expose
        public Integer item_name;
        @SerializedName("price")
        @Expose
        public Integer price;
        @SerializedName("category")
        @Expose
        public Integer category;
        @SerializedName("food_category")
        @Expose
        public Integer foodcategory;
        @SerializedName("purchase_date")
        @Expose
        public Integer purchaseDate;

        public Integer getId() {
            return id;
        }

        public Integer getEmail() {
            return email;
        }

        public Integer getItem_name() {
            return item_name;
        }

        public Integer getPrice() {
            return price;
        }

        public Integer getCategory() {
            return category;
        }

        public Integer getFoodcategory() {
            return foodcategory;
        }

        public Integer getPurchaseDate() {
            return purchaseDate;
        }
    }



}

