package com.example.hyehy.helpme.setting;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.hyehy.helpme.ExcelActivity;
import com.example.hyehy.helpme.InquiryActivity;
import com.example.hyehy.helpme.MainActivity;
import com.example.hyehy.helpme.R;
import com.example.hyehy.helpme.WithdrawalActivity;

public class MainBottomMenu extends BottomSheetDialogFragment {
    private BottomSheetListener mListener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_setting_menu, container, false);

        TextView tvUserEmail = v.findViewById(R.id.user_name); //사용자계정
        Button menu1 = v.findViewById(R.id.btn_1); //문의
        Button menu2 = v.findViewById(R.id.btn_2); //탈퇴
        Button menu3 = v.findViewById(R.id.btn_3); //로그아웃
        Button menu4 = v.findViewById(R.id.btn_4); //엑셀



        //문의
        menu1.setOnClickListener(view -> {
            Intent intent = new Intent(getActivity(), InquiryActivity.class);
            startActivity(intent);
            dismiss();
        });

        //엑셀파일 저장하기
        menu4.setOnClickListener(view -> {
            Intent intent = new Intent(getActivity(), ExcelActivity.class);
            startActivity(intent);
            dismiss();
        });


        //탈퇴
        menu2.setOnClickListener(view -> {
            Intent intent = new Intent(getActivity(), WithdrawalActivity.class);
            startActivity(intent);

            dismiss();
        });


        //로그아웃
        menu3.setOnClickListener(view -> {
//            Intent intent = new Intent(getActivity(), MainActivity.class);
//            startActivity(intent);

            dismiss();
        });

        return v;
    }

    public interface BottomSheetListener {
        void onButtonClicked(String msg);
        void resetLocation();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mListener = (BottomSheetListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException();
        }
    }
}
