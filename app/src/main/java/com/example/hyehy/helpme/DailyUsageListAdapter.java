package com.example.hyehy.helpme;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class DailyUsageListAdapter extends BaseAdapter {
    Context context;
    ArrayList<DailyUsageList> list_dailyUsage;

    TextView tvCategoryName;
    TextView tvCategoryCost;

    public DailyUsageListAdapter(Context context, ArrayList<DailyUsageList> list_dailyUsage) {
        this.context = context;
        this.list_dailyUsage = list_dailyUsage;
    }


    @Override
    public int getCount() {
        return this.list_dailyUsage.size();
    }

    @Override
    public Object getItem(int position) {
        return list_dailyUsage.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        convertView = LayoutInflater.from(context).inflate(R.layout.item_daily_list, null);

        tvCategoryName = convertView.findViewById(R.id.daily_list_name);
        tvCategoryCost = convertView.findViewById(R.id.daily_list_cost);

        tvCategoryName.setText(list_dailyUsage.get(position).getDailyListName());
        tvCategoryCost.setText(list_dailyUsage.get(position).getDailyListCost());


        return convertView;
    }
}
