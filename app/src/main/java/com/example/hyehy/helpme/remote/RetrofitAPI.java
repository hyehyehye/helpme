package com.example.hyehy.helpme.remote;

import com.example.hyehy.helpme.common.NetworkInterceptor;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitAPI {
    private static final String BASE_URL = "http://52.78.16.243:7001/"; // 서버 API


    private static Retrofit getRetrofitInstance() {
        NetworkInterceptor interceptor = new NetworkInterceptor();
        interceptor.setLevel(NetworkInterceptor.Level.BODY);

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .connectTimeout(100, TimeUnit.SECONDS)
                .readTimeout(100, TimeUnit.SECONDS)
                .build();

        Gson gson = new GsonBuilder().setLenient().create();
        return new Retrofit
                .Builder()
                .baseUrl(BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }

    public static Request getApICall() {

        return getRetrofitInstance()
                .create(Request.class);
    }

//    private static Retrofit getretrofitInstance = new Retrofit.Builder()
//            .baseUrl(BASE_URL)
//            .addConverterFactory(GsonConverterFactory.create())
//            .build();
//
//    public static Request getApICall(){
//        return getretrofitInstance.create(Request.class);
//    }

}


